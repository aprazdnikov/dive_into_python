

class SomeObject:
    def __init__(self):
        self.integer_field = 0
        self.float_field = 0.0
        self.string_field = ""


obj = SomeObject()


class EventGet:
    def __init__(self, value):
        self.kind = value


class EventSet:
    def __init__(self, value):
        self.kind = value


class NullHandler:
    def __init__(self, successor=None):
        self.__successor = successor

    def handle(self, obj_, event):
        if self.__successor is not None:
            return self.__successor.handle(obj_, event)


class IntHandler(NullHandler):
    def handle(self, obj_, event):
        if event.kind == int:
            print(f"Get {obj_.integer_field}")
            return obj_.integer_field
        elif type(event.kind) == int:
            obj_.integer_field = event.kind
            print(f"Set {obj_.integer_field}")
        else:
            print("Передаю событие дальше")
            return super().handle(obj_, event)


class FloatHandler(NullHandler):
    def handle(self, obj_, event):
        if event.kind == float:
            print(f"Get {obj_.float_field}")
            return obj_.float_field
        elif type(event.kind) == float:
            obj_.float_field = event.kind
            print(f"Set {obj_.float_field}")
        else:
            print("Передаю событие дальше")
            return super().handle(obj_, event)


class StrHandler(NullHandler):
    def handle(self, obj_, event):
        if event.kind == str:
            print(f"Get {obj_.string_field}")
            return obj_.string_field
        elif type(event.kind) == str:
            obj_.string_field = event.kind
            print(f"Set {obj_.string_field}")
        else:
            print("Передаю событие дальше")
            return super().handle(obj_, event)


chain = IntHandler(FloatHandler(StrHandler(NullHandler())))
chain.handle(obj, EventGet(int))  # вернуть значение obj.integer_field
chain.handle(obj, EventGet(str))  # вернуть значение obj.string_field
chain.handle(obj, EventGet(float))  # вернуть значение obj.float_field
chain.handle(obj, EventSet(1))  # установить значение obj.integer_field = 1
chain.handle(obj, EventSet(1.1))  # установить значение obj.float_field = 1.1
chain.handle(obj, EventSet("str"))  # установить значение obj.string_field = "str"
