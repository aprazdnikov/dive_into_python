

_DICT_TYPE = {int: "INT", float: "FLOAT", str: "STR"}


class SomeObject:
    def __init__(self):
        self.integer_field = 0
        self.float_field = 0.0
        self.string_field = ""


obj = SomeObject()


class EventGet:
    def __init__(self, type_):
        self.kind = _DICT_TYPE[type_]
        self.value = None


class EventSet:
    def __init__(self, value):
        self.kind = _DICT_TYPE[type(value)]
        self.value = value


class NullHandler:
    def __init__(self, successor=None):
        self.__successor = successor

    def handle(self, obj, event):
        if self.__successor is not None:
            return self.__successor.handle(obj, event)


class IntHandler(NullHandler):
    def handle(self, obj, event):
        if event.kind == _DICT_TYPE[int]:
            if event.value is None:
                print(f"Get {obj.integer_field}")
                return obj.integer_field
            else:
                obj.integer_field = event.value
                print(f"Set {obj.integer_field}")
        else:
            print("Передаю событие дальше")
            return super().handle(obj, event)


class FloatHandler(NullHandler):
    def handle(self, obj, event):
        if event.kind == _DICT_TYPE[float]:
            if event.value is None:
                print(f"Get {obj.float_field}")
                return obj.float_field
            else:
                obj.float_field = event.value
                print(f"Set {obj.float_field}")
        else:
            print("Передаю событие дальше")
            return super().handle(obj, event)


class StrHandler(NullHandler):
    def handle(self, obj, event):
        if event.kind == _DICT_TYPE[str]:
            if event.value is None:
                print(f"Get {obj.string_field}")
                return obj.string_field
            else:
                obj.string_field = event.value
                print(f"Set {obj.string_field}")
        else:
            print("Передаю событие дальше")
            return super().handle(obj, event)


chain = IntHandler(FloatHandler(StrHandler(NullHandler())))
chain.handle(obj, EventGet(int))  # вернуть значение obj.integer_field
chain.handle(obj, EventGet(str))  # вернуть значение obj.string_field
chain.handle(obj, EventGet(float))  # вернуть значение obj.float_field
chain.handle(obj, EventSet(1))  # установить значение obj.integer_field = 1
chain.handle(obj, EventSet(1.1))  # установить значение obj.float_field = 1.1
chain.handle(obj, EventSet("str"))  # установить значение obj.string_field = "str"
