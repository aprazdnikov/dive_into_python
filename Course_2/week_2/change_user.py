import math
import random

import pygame

SCREEN_DIM = (800, 600)

class Vec2d:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vec2d(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vec2d(self.x - other.x, self.y - other.y)

    def __mul__(self, k):
        return Vec2d(self.x * k, self.y * k)

    def __truediv__(self, k):
        return Vec2d(self.x / k, self.y / k)

    def __len__(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    @property
    def int_pair(self):
        return int(self.x), int(self.y)


class Polyline:

    def __init__(self, pygame, gameDisplay, dimension):
        self.points = []
        self.speeds = []
        self.display = gameDisplay
        self.pygame = pygame
        self.dimension = dimension
        self.max_speed = 100
        self.min_speed = 0.0005

    def add_point(self, point, speed):
        """Add point on screen and init her speed."""
        self.points.append(point)
        self.speeds.append(speed)

    def del_point(self):
        """Remove last (point and speed) from screen."""
        if self.points and self.speeds:
            self.points.pop()
            self.speeds.pop()

    def speed_up(self):
        """Speed up last added point."""
        if self.speeds:
            speed = self.speeds.pop()
            if abs(speed.x) < self.max_speed and abs(speed.y) < self.max_speed:
                self.speeds.append(speed * 2)
            else:
                self.speeds.append(speed)

    def speed_down(self):
        """Speed down last added point."""
        if self.speeds:
            speed = self.speeds.pop()
            if abs(speed.x) > self.min_speed and abs(speed.y) > self.min_speed:
                self.speeds.append(speed / 2)
            else:
                self.speeds.append(speed)

    def set_points(self):
        """Recounting reference point coordinates."""
        for p in range(len(self.points)):
            self.points[p] = self.points[p] + self.speeds[p]

            if self.points[p].x > self.dimension[0] or self.points[p].x < 0:
                self.speeds[p].x = -self.speeds[p].x

            if self.points[p].y > self.dimension[1] or self.points[p].y < 0:
                self.speeds[p].y = -self.speeds[p].y

    def draw_points(self, style='points', width=3, color=(255, 255, 255)):
        """Draw points or line."""
        if style == 'line':
            for p_n in range(-1, len(self.points) - 1):
                self.pygame.draw.line(self.display,
                                      color,
                                      self.points[p_n].int_pair,
                                      self.points[p_n + 1].int_pair,
                                      width)

        elif style == 'points':
            for p in self.points:
                self.pygame.draw.circle(self.display,
                                        color,
                                        p.int_pair,
                                        width)


class Knot(Polyline):

    def __init__(self, polyline, steps):
        super().__init__(polyline.pygame, polyline.display, polyline.dimension)
        self.steps = steps
        self.polyline = polyline

    def draw_points(self, style, color):
        self.get_knot()
        super().draw_points(style=style, color=color)

    def get_knot(self):
        self.points = []
        points = self.polyline.points

        if len(points) < 3:
            return []

        for i in range(-2, len(points) - 2):
            ptn = []
            ptn.append((points[i] + points[i+1]) * 0.5)
            ptn.append(points[i+1])
            ptn.append((points[i+1] + points[i+2]) * 0.5)
            self.points.extend([p for p in self.get_points(ptn)])

    def get_points(self, base_points):
        alpha = 1 / self.steps
        res = []
        for i in range(self.steps):
            res.append(self.get_point(base_points, i * alpha))
        return res

    def get_point(self, base_points, alpha, deg=None):
        if deg is None:
            deg = len(base_points) - 1

        if deg == 0:
            return base_points[0]

        points = base_points[deg] * alpha

        return points+self.get_point(base_points, alpha, deg - 1) * (1 - alpha)


def draw_help(steps):
    """Draw HELP menu if press F1 button."""
    gameDisplay.fill((50, 50, 50))
    font1 = pygame.font.SysFont("courier", 24)
    font2 = pygame.font.SysFont("serif", 24)
    data = []
    data.append(["F1", "Show Help"])
    data.append(["R", "Restart"])
    data.append(["P", "Pause/Play"])
    data.append(["Num+", "More points"])
    data.append(["Num-", "Less points"])
    data.append(["Left Mouse", "ADD point"])
    data.append(["Right Mouse", "DEL point"])
    data.append(["Scroll UP", "Speed UP"])
    data.append(["Scroll Down", "Speed down"])
    data.append(["", ""])
    data.append([str(steps), "Current points"])

    pygame.draw.lines(gameDisplay, (255, 50, 50, 255), True,
                      [(0, 0), (800, 0), (800, 600), (0, 600)], 5)
    for i, text in enumerate(data):
        gameDisplay.blit(font1.render(
            text[0], True, (128, 128, 255)), (100, 100 + 30 * i))
        gameDisplay.blit(font2.render(
            text[1], True, (128, 128, 255)), (300, 100 + 30 * i))


if __name__ == "__main__":
    pygame.init()
    gameDisplay = pygame.display.set_mode(SCREEN_DIM)
    pygame.display.set_caption("MyScreenSaver")

    steps = 35
    working = True
    polyline = Polyline(pygame, gameDisplay, SCREEN_DIM)
    knot = Knot(polyline, steps)
    show_help = False
    pause = True

    hue = 0
    color = pygame.Color(0)

    while working:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                working = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    working = False
                if event.key == pygame.K_r:
                    polyline = Polyline()
                    knot = Knot(polyline, steps)
                if event.key == pygame.K_p:
                    pause = not pause
                if event.key == pygame.K_KP_PLUS:
                    knot.steps += 1
                if event.key == pygame.K_F1:
                    show_help = not show_help
                if event.key == pygame.K_KP_MINUS:
                    knot.steps -= 1 if knot.steps > 1 else 0

            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos
                if event.button == 1:
                    polyline.add_point(
                        point=Vec2d(x, y),
                        speed=Vec2d(random.random() * 2, random.random() * 2))
                elif event.button == 3:
                    polyline.del_point()
                elif event.button == 4:
                    polyline.speed_up()
                elif event.button == 5:
                    polyline.speed_down()

        gameDisplay.fill((0, 0, 0))
        hue = (hue + 1) % 360
        color.hsla = (hue, 100, 50, 100)
        polyline.draw_points()
        knot.draw_points("line", color)
        if not pause:
            polyline.set_points()
        if show_help:
            draw_help(knot.steps)

        pygame.display.flip()

    pygame.display.quit()
    pygame.quit()
    exit(0)