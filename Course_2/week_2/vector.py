import pygame
import random
import math

SCREEN_DIM = (800, 600)


class Vec2d(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, vec):
        return Vec2d(self.x + vec.x, self.y + vec.y)

    def __sub__(self, vec):
        return Vec2d(self.x - vec.x, self.y - vec.y)

    def __mul__(self, k):
        return Vec2d(self.x * k, self.y * k)

    def __len__(self):
        return float(math.sqrt(x.x ** 2 + x.y ** 2))

    def int_pair(self):
        return int(self.x), int(self.y)


class Polyline(object):
    def __init__(self, pygame_, display, screen_dim):
        self.points = []
        self.speeds = []
        self.pygame = pygame_
        self.display = display
        self.screen_dim = screen_dim

    def add_point(self, point, speed):
        self.points.append(point)
        self.speeds.append(speed)

    def set_points(self):
        for p in range(len(self.points)):
            self.points[p] += self.speeds[p]
            if self.points[p].x > self.screen_dim[0] or self.points[p].x < 0:
                self.speeds[p] = Vec2d(- self.speeds[p].x, self.speeds[p].y)
            if self.points[p].y > self.screen_dim[1] or self.points[p].y < 0:
                self.speeds[p] = Vec2d(self.speeds[p].x, -self.speeds[p].y)

    def draw_points(self, width=3, color_=(255, 255, 255)):
        for point in self.points:
            self.pygame.draw.circle(
                self.display, color_, point.int_pair(), width
            )


class Knot(Polyline):
    def __init__(self, polyline, steps):
        super().__init__(polyline.pygame,
                         polyline.display, polyline.screen_dim)
        self.polyline = polyline
        self.steps = steps

    def get_point(self, points, alpha, deg=None):
        if deg is None:
            deg = len(points) - 1
        if deg == 0:
            return points[0]
        return points[deg]*alpha + self.get_point(
            points, alpha, deg-1
        ) * (1-alpha)

    def get_points(self, base_points):
        alpha = 1 / self.steps
        res = []
        for i in range(self.steps):
            res.append(self.get_point(base_points, i*alpha))
        return res

    def get_knot(self):
        if len(self.polyline.points) < 3:
            return []
        else:
            points = self.polyline.points
            res = []
            for i in range(-2, len(points) - 2):
                ptn = []
                ptn.append((points[i] + points[i + 1]) * 0.5)
                ptn.append(points[i + 1])
                ptn.append((points[i + 1] + points[i + 2]) * 0.5)
                res.extend(self.get_points(ptn))
            return res

    def draw_points(self, width=3, color_=(255, 255, 255)):
        points = self.get_knot()
        for p_ in range(-1, len(points)-1):
            self.pygame.draw.line(
                self.display,
                color,
                points[p_].int_pair(),
                points[p_+1].int_pair(),
                width
            )


# Отрисовка справки
def draw_help(steps):
    gameDisplay.fill((50, 50, 50))
    font1 = pygame.font.SysFont("courier", 24)
    font2 = pygame.font.SysFont("serif", 24)
    data = []
    data.append(["F1", "Show Help"])
    data.append(["R", "Restart"])
    data.append(["P", "Pause/Play"])
    data.append(["Num+", "More points"])
    data.append(["Num-", "Less points"])
    data.append(["", ""])
    data.append([str(steps), "Current points"])

    pygame.draw.lines(gameDisplay, (255, 50, 50, 255), True, [
                      (0, 0), (800, 0), (800, 600), (0, 600)], 5)
    for i, text in enumerate(data):
        gameDisplay.blit(font1.render(
            text[0], True, (128, 128, 255)), (100, 100 + 30 * i))
        gameDisplay.blit(font2.render(
            text[1], True, (128, 128, 255)), (200, 100 + 30 * i))


# Основная программа
if __name__ == "__main__":
    pygame.init()
    gameDisplay = pygame.display.set_mode(SCREEN_DIM)
    pygame.display.set_caption("MyScreenSaver")

    steps = 35
    working = True
    polyline = Polyline(pygame, gameDisplay, SCREEN_DIM)
    knot = Knot(polyline, steps)
    show_help = False
    pause = True

    hue = 0
    color = pygame.Color(0)

    while working:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                working = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    working = False
                if event.key == pygame.K_r:
                    polyline = Polyline(pygame, gameDisplay, SCREEN_DIM)
                    knot = Knot(polyline, steps)
                if event.key == pygame.K_p:
                    pause = not pause
                if event.key == pygame.K_KP_PLUS:
                    knot.steps += 1
                if event.key == pygame.K_F1:
                    show_help = not show_help
                if event.key == pygame.K_KP_MINUS:
                    knot.steps -= 1 if knot.steps > 1 else 0

            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos
                polyline.add_point(
                    point=Vec2d(x, y),
                    speed=Vec2d(random.random() * 2, random.random() * 2)
                )

        gameDisplay.fill((0, 0, 0))
        hue = (hue + 1) % 360
        color.hsla = (hue, 100, 50, 100)
        polyline.draw_points()
        knot.draw_points(3, color)
        if not pause:
            polyline.set_points()
            knot.set_points()
        if show_help:
            draw_help(knot.steps)

        pygame.display.flip()

    pygame.display.quit()
    pygame.quit()
    exit(0)
