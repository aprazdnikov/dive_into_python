import sys
import unittest


def factorize(x):
    """ Factorize positive integer and return its factors.
        :type x: int,>=0
        :rtype: tuple[N],N>0
    """
    pass


class TestFactorize(unittest.TestCase):
    def test_wrong_types_raise_exception(self):
        """
        Что типы float и str (значения 'string', 1.5)
        вызывают исключение TypeError.
        """
        self.cases = ('string', 1.5)
        for case in self.cases:
            with self.subTest(x=case):
                self.assertRaises(TypeError, factorize, case)

    def test_negative(self):
        """
        Что для отрицательных чисел -1, -10 и -100
        вызывается исключение ValueError.
        """
        self.cases = (-1, -10, -100)
        for case in self.cases:
            with self.subTest(x=case):
                self.assertRaises(ValueError, factorize, case)

    def test_zero_and_one_cases(self):
        """
        Что для числа 0 возвращается кортеж (0,), а для числа 1 кортеж (1,)
        """
        self.cases = (0, 1)
        for case in self.cases:
            with self.subTest(x=case):
                self.assertTupleEqual(factorize(case), (case, ))

    def test_simple_numbers(self):
        """
        Что для простых чисел 3, 13, 29 возвращается кортеж,
        содержащий одно данное число.
        """
        self.cases = (3, 13, 29)
        for case in self.cases:
            with self.subTest(x=case):
                self.assertTupleEqual(factorize(case), (case,))

    def test_two_simple_multipliers(self):
        """
        Что для чисел 6, 26, 121
        возвращаются соответственно кортежи (2, 3), (2, 13) и (11, 11).
        """
        self.cases = (6, 26, 121)
        expected = ((2, 3), (2, 13), (11, 11))
        for i, case in enumerate(self.cases):
            with self.subTest(x=case):
                self.assertTupleEqual(factorize(case), expected[i])

    def test_many_multipliers(self):
        """
        Что для чисел 1001 и 9699690 возвращаются соответственно
        кортежи (7, 11, 13) и (2, 3, 5, 7, 11, 13, 17, 19).
        """
        self.cases = (1001, 9699690)
        expected = ((7, 11, 13), (2, 3, 5, 7, 11, 13, 17, 19))
        for i, case in enumerate(self.cases):
            with self.subTest(x=case):
                self.assertTupleEqual(factorize(case), expected[i])


# При этом несколько различных проверок в рамках одного теста
# должны быть обработаны как подслучаи с указанием x: subTest(x=...).


def factorize_test_suit():
    suite = unittest.TestSuite()
    suite.addTest(TestFactorize('test_wrong_types_raise_exception'))
    suite.addTest(TestFactorize('test_negative'))
    suite.addTest(TestFactorize('test_zero_and_one_cases'))
    suite.addTest(TestFactorize('test_simple_numbers'))
    suite.addTest(TestFactorize('test_two_simple_multipliers'))
    suite.addTest(TestFactorize('test_many_multipliers'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner(stream=sys.stdout, verbosity=2)

    print('Testing function ', factorize.__doc__.strip())
    test_suite = factorize_test_suit()
    runner.run(test_suite)
