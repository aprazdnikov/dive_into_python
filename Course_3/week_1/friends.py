import requests
from datetime import datetime
from operator import itemgetter

YEAR = datetime.now().year

URL_METHOD = 'https://api.vk.com/method/'

ACCESS_TOKEN = \
    'd6c01b0dd6c01b0dd6c01b0de7d6a973aadd6c0d6c01b0d8a53166edb9c99245ecdb9bd'

VER_API = '5.71'


def calc_age(uid='reigning'):
    id_ = get_user_id(uid)
    return get_list_users(id_)


def get_user_id(uid):
    r = requests.get(
        url=URL_METHOD + 'users.get',
        params={
            'v': VER_API,
            'access_token': ACCESS_TOKEN,
            'user_ids': uid
        }
    )
    return r.json()['response'][0]['id']


def get_list_users(id_, fields='bdate'):
    r = requests.get(
        url=URL_METHOD + 'friends.get',
        params={
            'v': VER_API,
            'access_token': ACCESS_TOKEN,
            'user_id': id_,
            'fields': fields
        }
    )

    list_old = []
    for year in get_year_user(r.json()['response']['items']):
        if year:
            list_old.append(YEAR - year)

    return get_list_tuple(list_old)


def get_year_user(list_user):
    for user in list_user:
        if user.get('bdate') is not None:
            year = parse_date(user['bdate'])
            if year is not None:
                yield year


def parse_date(s, fmt='%d.%m.%Y'):
    try:
        date = datetime.strptime(s, fmt)
    except ValueError as error:
        return None
    return date.year


def get_list_tuple(list_old):
    set_old = set(list_old)
    list_tuple = []

    for old in set_old:
        list_tuple.append((old, list_old.count(old)))
    s = sorted(list_tuple, key=itemgetter(0))
    return sorted(s, key=itemgetter(1), reverse=True)


if __name__ == '__main__':
    res = calc_age()
    if res is not None:
        print(res)
