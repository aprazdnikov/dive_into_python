from bs4 import BeautifulSoup
from decimal import Decimal


def convert(amount, cur_from, cur_to, date, requests):
    response = requests.get(
        "http://www.cbr.ru/scripts/XML_daily.asp",
        params={
            'date_req': date
        }
    )
    soup = BeautifulSoup(response.content, "xml")

    if cur_from == 'RUR':
        convert_amount = amount
    else:
        in_valute = soup.find('CharCode', text=cur_from)
        in_valute_value = Decimal(
            in_valute.find_next_sibling('Value').string.replace(',', '.')
        )
        in_valute_nominal = Decimal(
            in_valute.find_next_sibling('Nominal').string
        )
        in_valute = in_valute_value / in_valute_nominal
        convert_amount = in_valute * amount

    to_valute = soup.find('CharCode', text=cur_to)
    to_valute_value = Decimal(
        to_valute.find_next_sibling('Value').string.replace(',', '.')
    )
    to_valute_nominal = Decimal(
        to_valute.find_next_sibling('Nominal').string
    )
    to_valute = to_valute_value / to_valute_nominal

    result = convert_amount / to_valute
    result = result.quantize(Decimal('1.0000'))

    return result  # не забыть про округление до 4х знаков после запятой
