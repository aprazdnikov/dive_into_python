from functools import wraps
from json import dumps


# пример декоратора
def to_json(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        return dumps(func(*args, **kwargs))

    return wrapper


t = [1, 2, 3, 4, 5]
print(t[5:])
