import sys


def get_solution():

    digit_string = sys.argv[1]

    total = 0

    for i in digit_string:
        if i.isdigit():
            total += int(i)

    return total


if __name__ == "__main__":
    print(get_solution())
