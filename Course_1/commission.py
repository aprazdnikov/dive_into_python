

class Value(object):
    def __init__(self):
        self.ammount = 0

    def __get__(self, instance, owner):
        return self.ammount

    def __set__(self, instance, value):
        self.ammount = value - (instance.commission * value)


class Account(object):
    ammount = Value()

    def __init__(self, commission):
        self.commission = commission


if __name__ == '__main__':
    new_account = Account(0.1)
    new_account.ammount = 100
    print(new_account.ammount)
