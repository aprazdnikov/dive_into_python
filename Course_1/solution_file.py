import os

_FILE = os.path.join(os.path.dirname(__file__), 'example.txt')


class FileReader(object):
    def __init__(self, file):
        self.file = file

    def read(self):
        try:
            with open(self.file, 'r') as f:
                return_data = f.readline()
        except IOError as err:
            # print(err.strerror)
            return_data = ''

        return return_data


if __name__ == '__main__':
    reader = FileReader(_FILE)
    print(reader.read())
