import sys


def get_square_root():

    a = sys.argv[1]
    b = sys.argv[2]
    c = sys.argv[3]

    a = int(a)
    b = int(b)
    c = int(c)

    discr = b**2 - 4 * a * c

    if discr > 0:
        x_1 = (-b + discr**.5) / (2 * a)
        x_2 = (-b - discr**.5) / (2 * a)
        print("%.0f \n%.0f" % (x_1, x_2))
    elif discr == 0:
        x = -b / (2 * a)
        print("%.2f" % x)
    else:
        print("Корней нет")


if __name__ == "__main__":
    get_square_root()
