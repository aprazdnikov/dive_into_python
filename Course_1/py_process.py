import asyncio


async def hello():
    while True:
        print("Hello!")
        await asyncio.sleep(1.0)


loop = asyncio.get_event_loop()
loop.run_until_complete(hello())
loop.close()