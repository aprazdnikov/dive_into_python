import sys


def get_num_steps():

    num_steps = sys.argv[1]

    if num_steps.isdigit():
        num_steps = int(num_steps)
        for i in range(1, num_steps+1, 1):
            print((f' '*(num_steps-i)) + (f'#'*i))


if __name__ == "__main__":
    get_num_steps()
