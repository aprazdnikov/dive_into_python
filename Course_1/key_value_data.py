from tempfile import gettempdir
from argparse import ArgumentParser
from json import loads, dumps
from os import path
from functools import wraps

STORAGE_DATA_PATH = path.join(gettempdir(), 'storage.data')


def to_json(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        return dumps(func(*args, **kwargs))

    return wrapper


def _get_storage_data():
    if not path.exists(STORAGE_DATA_PATH):
        return {}

    with open(STORAGE_DATA_PATH, 'r') as f:
        _data = f.read()
        if _data:
            return loads(_data)

        return {}


def pop(key, value):
    data = _get_storage_data()

    if key in data:
        data[key].append(value)
    else:
        data[key] = [value]

    with open(STORAGE_DATA_PATH, 'w') as f:
        f.write(dumps(data))


@to_json
def get(key):
    data = _get_storage_data()
    return data.get(key)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        "-k", "--key",
        help="Ключ по которому можно получить или записать данные"
    )
    parser.add_argument(
        "-v", "--value", help="Значение для записи, если указано"
    )
    args = parser.parse_args()

    if args.key and args.value:
        pop(args.key, args.value)
    elif args.key:
        print(get(args.key))
